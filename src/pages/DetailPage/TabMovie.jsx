import React from "react";
import { Button, Tabs } from "antd";

export default function TabMovie(props) {
  let { heThongRapChieu } = props.data;
  return (
    <div className="w-full">
      <Tabs defaultActiveKey="0">
        {heThongRapChieu?.map((heThongRap) => {
          return (
            <Tabs.TabPane
              tab={<img className="w-20" src={heThongRap.logo} alt="" />}
              key={heThongRap.maHeThongRap}
            >
              <Tabs defaultActiveKey="0">
                {heThongRap.cumRapChieu?.map((cumRap) => {
                  return (
                    <Tabs.TabPane
                      tab={<p>{cumRap.tenCumRap}</p>}
                      key={cumRap.maCumRap}
                    >
                      <div className="w-full text-left">
                        {cumRap.lichChieuPhim?.map((lichChieu) => {
                          return (
                            <Button
                              key={lichChieu.maLichChieu}
                              danger
                              type="primary"
                            >
                              {lichChieu.ngayChieuGioChieu}
                            </Button>
                          );
                        })}
                      </div>
                    </Tabs.TabPane>
                  );
                })}
              </Tabs>
            </Tabs.TabPane>
          );
        })}
      </Tabs>
    </div>
  );
}
